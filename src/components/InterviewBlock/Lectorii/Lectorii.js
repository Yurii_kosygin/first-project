import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Lectorii.styl';
import srcAuthor from 'assets/author/lectorii_1.jpg';

let b = block('lectorii');

export default class Lectorii extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      const item = this.props.filler
      return (
          <div className={b}>
            <div className={b('title')}>
              <span className={b('subject')}>{item.subject}</span>
              <div className={b('block-date')}>
                <div className={b('date-divider')}></div>
                <span className={b('date-number')}>{item.date_number}</span>
                <span className={b('date-month')}>{item.date_month}</span>
              </div>
            </div>
            <div className={b('img')}><div className={b('mask')}></div><img src={srcAuthor}/></div>
            <p className={b('text')}>{item.text}</p>
            <div className={b('divider')}></div>
            <span className={b('author')}>Лектор &mdash; <b>{item.author}</b></span>
          </div>
      )
    } 
}