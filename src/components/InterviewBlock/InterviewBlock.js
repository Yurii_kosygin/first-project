import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './InterviewBlock.styl';
import { Interview } from 'components';
import Lectorii from './Lectorii/Lectorii'
import LectoriiBanner from './LectoriiBanner/LectoriiBanner'

let b = block('interview-block');
const interview = 
  {
    subject: 'Интервью',
    img: '',
    title: 'Математика в школе превратилась в пыточную систему',
    author: 'Алексей Семихатов',
    text: 'Доктором физико-математических наук, ведущим программ «На грани безумия» на канале ОТР и «Вопрос науки» на канале «Наука 2.0»'
  }
const lectorii = 
  {
    subject: 'Лекторий',
    date_number: '22',
    date_month: 'июня',
    img: '',
    text: 'Миллионный мост и загадки воды — первая летняя экскурсия от Школы молодого москвича в этом сезоне!',
    author: 'Наталья Крылова'
  }

export default class InterviewBlock extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
          <div className={b}>
            <Interview filler={interview}/>
            <Lectorii  filler={lectorii}/>
            <LectoriiBanner />
          </div>
      )
    } 
}