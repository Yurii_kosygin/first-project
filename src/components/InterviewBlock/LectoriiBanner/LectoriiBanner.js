import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './LectoriiBanner.styl';
import src from 'assets/banners/banner.jpg';

let b = block('lectorii-banner');

export default class LectoriiBanner extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
          <div className={b}>
            <div className={b('img')}><img src={src}/></div>
          </div>
      )
    } 
}