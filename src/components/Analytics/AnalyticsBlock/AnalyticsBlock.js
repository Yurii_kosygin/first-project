import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './AnalyticsBlock.styl';
import src from 'assets/banners/analitic.jpg';

let b = block('analytics-block');

export default class AnalyticsBlock extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      const item = this.props.item
      return (
          <div className={b}>
            <span className={b('subject')}>{item.subject}</span>
            <div className={b('img')}><div className={b('mask')}></div><img src={src}/></div>
            <span className={b('title')}>{item.title}</span>
            <p className={b('text')}>{item.text}</p>
          </div>
      )
    } 
}