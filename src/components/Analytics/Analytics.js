import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Analytics.styl';
import AnalyticsBlock from './AnalyticsBlock/AnalyticsBlock';

let b = block('analytics');

const analytics = [
      {
        subject: 'Аналитика', 
        img: '',
        title: 'ОЗК просит запретить схему незаконного экспорта зерна из РФ',
        text: 'Указывается, что в водах Керченского пролива сложилась «незаконная схема экспорта», ущерб от которой оценивается в $1,2 млрд'
      },
      {
        subject: 'Аналитика', 
        img: '',
        title: 'Крупнейший в стране завод иммунопрепаратов за 2 млрд рублей открылся под Владимиром',
        text: 'Производство сможет обеспечить потребность страны в препаратах для диагностики туберкулеза, а также откроет возможности экспорта'
      }
    ]

export default class Analytics extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
          <div className={b}>
            <div className={b('container')}>
              <AnalyticsBlock item={analytics[0]}/>
              <AnalyticsBlock item={analytics[1]}/>
            </div>
          </div>
      )
    } 
}