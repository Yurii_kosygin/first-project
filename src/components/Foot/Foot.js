import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Foot.styl';
import CallBack from './CallBack/CallBack';
import Info from './Info/Info';

let b = block('foot');

export default class Foot extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
            <div className={b}>
              <Info />
              <CallBack />
              <span className={b('copy')}><b>&copy; 2016  </b>Культурный Минимум</span>
            </div>
      )
    } 
}