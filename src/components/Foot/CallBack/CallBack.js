import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './CallBack.styl';

let b = block('call-back');

export default class CallBack extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
            <form className={b}>
              <p className={b('text')}>Подписывайтесь и получайте новые материалы первыми</p>
              <input className={b('input')} type="text" placeholder="Ваш e-mail"/>
              <button className={b('button')}>Подписаться</button>
            </form>
      )
    } 
}