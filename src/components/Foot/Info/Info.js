import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Info.styl';
import mn from 'assets/logo/mn.png';
import src from 'assets/logo/12.png';

let b = block('info');

export default class Info extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
            <div className={b}>
              <div className={b('text-block')}>
                <div className={b('img')}><img src={src}/></div>
                <p className={b('text')}>Сайт может содержать контент, не предназначенный для лиц младше 12-ти лет. Публикация материалов возможна только с согласия редакции.</p>
              </div>
              <div className={b('logo')}><img src={mn}/></div>
            </div>
      )
    } 
}