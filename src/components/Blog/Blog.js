import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Blog.styl';
import src from 'assets/banners/blog_1.jpg';
import srcAuthor from 'assets/author/blog.jpg';


let b = block('blog');

export default class Blog extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      const item = this.props.item
      return (
          <div className={b}>
            <a className={b('link')} href='/'><b>Блоги</b></a>
            <div className={b('block')}>
              <div className={b('img')}><img src={src}/></div>
              <div className={b('text-block')}>
                <div className={b('title')}>{item.title}</div>
                <div className={b('text')}>{item.text}</div>
                <div className={b('author-block')}>
                  <div className={b('author-img')}><img src={srcAuthor}/></div>
                  <div className={b('author-text')}>
                    <b className={b('author-name')}>{item.author}</b>
                    <span className={b('author-position')}>{item.author_position}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
      )
    } 
}