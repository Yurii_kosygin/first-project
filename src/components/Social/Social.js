import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Social.styl';

let b = block('social');

export default class Social extends Component {

  constructor(props) {
    super(props);

  }

    render() {
      return (
            <div className={b}>
              <span className={b('title')}>Мы в соцсетях</span>
              <div id="vk_groups">{VK.Widgets.Group("vk_groups", {mode: 3, width: "400", height: "160px"}, 20003922)}</div>
            </div>
      )
    } 
}