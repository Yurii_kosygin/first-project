import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './SearchBlock.styl';

let b = block('search-block');
let n = block('navigation');

const navItems = [
  {text: 'Блоги',href: '/'},
  {text: 'Лекторий',href: '/'},
  {text: 'Колонки', href: '/'},
  {text: 'Комментарии', href: '/'},
  {text: 'Интервью',href: '/'},
  {text: 'Аналитика',href: '/'}
]

export default class SearchBlock extends Component {

  constructor(props) {
    super(props);

	}
    render() {
      return (
          <div className={b}>
            <div>
              <div className={b('title')}>
                <span className={b('title-text')}>Культурный минимум</span>
              </div>
                <ul className={n}>
                  { navItems.map((item, key) => (
                      <li className={n('item')} key={key}><a className={n('link')} href={item.href}>{item.text}</a></li>
                    ))}
                </ul>
                  <a className={b('search')} href="/">Поиск</a>
            </div>
          </div>
      )
    }
}