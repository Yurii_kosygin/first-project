import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './NewsBlock.styl';

let b = block('news-block');
let l = block('news-list');

const listItems = [
  {text: 'Гергиева наградили медалью «За освобождение Пальмиры»',data: '23:10'},
  {text: 'В интернете появилась новая база данных автовладельцев',data: '22:25'},
  {text: 'Глава ФСБ рассказал о срыве в России терактов по «парижскому сценарию»', data: '09:14'}
]

export default class NewsBlock extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
          <div className={b}>
            <div>
              <div className={b('title')}>
                <span className={b('title-text')}>Новости дня</span>
              </div>
              <div className={l}>
                { listItems.map((item, key) => (
                  <div className={l('item')} key={key}>
                    <a href='/' className={l('text')}>{item.text}</a>
                    <span className={l('data')}>{item.data}</span>
                  </div>
                ))}
              </div>
              <a className={b('all-news')} href="/">Все новости</a>
            </div>
          </div>
      )
    }
}