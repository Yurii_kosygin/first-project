import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './HomeBlock.styl';
import Slider from 'react-slick';
import src from 'assets/banners/slide_1.jpg';
import srcAuthor from 'assets/author/slide_porof.png';
import NextBtn from 'material-ui/svg-icons/image/navigate-next'
import {grey50} from 'material-ui/styles/colors';


let b = block('slider-block');
const slideItems = [
      {text: 'На поддержку автопрома в 2016 году будет выделено 137,7 млрд рублей',imgUrl: 'http://placekitten.com/g/400/200', name: 'Иван Вальдшнеп', profil: 'доктор экономическийх  наук'},
      {text: 'На поддержку ',imgUrl: 'http://placekitten.com/g/400/200', name: 'Иван Вальдшнеп', profil: 'доктор экономическийх  наук'},
      {text: 'На поддержку автопрома в 2016 году будет выделено 137,7 млрд рублей',imgUrl: 'http://placekitten.com/g/400/200', name: 'Иван Вальдшнеп', profil: 'доктор экономическийх  наук'}
    ]
const iconStyles = {
  width: 20,
  height: 20,
  marginTop: 11,
};
class NextArrow extends Component {
  render() {
    return (
      <div {...this.props}><NextBtn style={iconStyles} color={grey50}/></div>
    )
  }
}

export default class HomeBlock extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      var settings = {
      customPaging: function(i) {
        return <a></a>
      },
      dots: true,
      arrows: true
    };
    
   
      return (
        <div className={b}>
          <span className={b('title')}>Главная</span>
          <Slider 
            nextArrow={(<NextArrow/>)}
            {...settings}>
              { slideItems.map((item, key) => (
                  <div className="slide" key={key} style={ { backgroundImage: `url(${src})` } }>
                    <div className={'mask'}></div>
                    <h1>{item.text}</h1>
                    <div className={b('profile')}>
                      <div className={b('img-profile')}><img src={srcAuthor}/></div>
                      <div className={b('text-profile')}>
                        <b>{item.name}</b>
                        <span>{item.profil}</span>
                      </div>
                    </div>
                  </div>
                ))}
          </Slider>
          <div className="divider"></div>
        </div>
      )
    }
}