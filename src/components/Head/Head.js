import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Head.styl';
import SearchBlock from './SearchBlock/SearchBlock';
import NewsBlock from './NewsBlock/NewsBlock';
import HomeBlock from './HomeBlock/HomeBlock';
let b = block('head');

export default class Head extends Component {

	constructor(props) {
		super(props);

	}
    render() {
    	return (
      		<div className={b}>
            <SearchBlock />
            <NewsBlock />
            <HomeBlock />
      		</div>
    	)
  	}	
}