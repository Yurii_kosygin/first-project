import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './CommentBlock.styl';
import src from 'assets/banners/comments_1.jpg';

let b = block('comment-block');

export default class CommentBlock extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      const item = this.props.item
      return (
          <div className={b}>
            <span className={b('subject')}>{item.subject}</span>
            <div className={b('img')}><div className={b('mask')}></div><img src={src}/></div>
            <b className={b('title')}>{item.title}</b>
            <p className={b('text')}>{item.text}</p>
          </div>
      )
    } 
}