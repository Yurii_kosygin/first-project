import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Comments.styl';
import { Interview } from 'components';
import CommentBlock from './CommentBlock/CommentBlock';

let b = block('comments');

const interview = 
  {
    subject: 'Интервью',
    img: '',
    title: 'Математика в школе превратилась в пыточную систему',
    author: 'Евгений Кривохарченко',
    text: 'Архитектор, художник, дизайнер, основатель и ректор архитектурной школы МАРШ, профессор МАРХИ'
  }
const comment = [
      {
        subject: 'Комментарий', 
        img: '',
        title: 'Как бороться с парковками в центре города, засильем развязок и маршрутками',
        text: 'В нашей программе развития данные позволяют получать информацию для принятия решений не только ведомствам и предприятиям, но и обычным людям.'
      },
      {
        subject: 'Комментарий', 
        img: '',
        title: 'Данные — это новое электричество, которое делает город умным. ',
        text: 'Платформа умной нации разработана как платформа обмена данными, а данные, которые известны всем, обладают силой и защищают от злоупотреблений бизнеса и правительства.'
      }
    ]

export default class Comments extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
          <div className={b}>
            <Interview filler={interview}/>
            <CommentBlock item={comment[0]}/>
            <CommentBlock item={comment[1]}/>
          </div>
      )
    } 
}