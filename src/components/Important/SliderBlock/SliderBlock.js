import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './SliderBlock.styl';
import Slider from 'react-slick';
import InnerSlide from './InnerSlide/InnerSlide'
import src from 'assets/banners/slide_2.jpg';

let b = block('slider');

const items = [
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'},
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'},
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'},
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'},
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'},
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'},
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'},
    {subject: 'Блог', imgUrl: '', title:'HeliRussia — 2016: что показали на международной вертолетной выставке', text: 'В этом году выставка собрала 219 компаний, в том числе 45 зарубежных из 16 стран'}
]
export default class SliderBlock extends Component {

  constructor(props) {
    super(props);
  }

    render() {
    var settings = {
      customPaging: function(i) {
        return <a></a>
      },
      dots: true,
      arrows: false,
      focusOnSelect: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1
    };
      return (
        <div className={b}>
        <div className ='wrapper'>
            <Slider 
              {...settings}>
                { items.map((item, key) => (
                    <div className='slide-two' key={key}>
                      <a>{item.subject}</a>
                      <div className='slide-two-img'><img src={src}/></div>
                      <h1>{item.title}</h1>
                      <p>{item.text}</p>
                    </div>
                  ))}
            </Slider>
          </div>
        </div>
      )
    } 
}