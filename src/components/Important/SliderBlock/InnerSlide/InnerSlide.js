import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './InnerSlide.styl';

let b = block('inner-slide');

export default class InnerSlide extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      const item = this.props.innerSlide
      console.log(item)
      return (
        <div className={b}>
          {item.title}
        </div>
      )
    } 
}