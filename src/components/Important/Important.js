import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Important.styl';
import SliderBlock from './SliderBlock/SliderBlock';

let b = block('important');

export default class Important extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      return (
            <div className={b}>
              <b className={b('title')}>Важное</b>
              <SliderBlock />
            </div>
      )
    } 
}