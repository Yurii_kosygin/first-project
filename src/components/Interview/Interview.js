import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './Interview.styl';
import srcAuthor from 'assets/author/inter_1.jpg';

let b = block('interview');

export default class Interview extends Component {

  constructor(props) {
    super(props);

  }
    render() {
      const filler = this.props.filler
      return (
          <div className={b}>
            <span className={b('subject')}>{filler.subject}</span>
            <div className={b('image')}><div className={b('mask')}></div><img src={srcAuthor}/></div>
            <div className={b('title-block')}>
              <h2 className={b('title')}>{filler.title}</h2>
              <div className={b('divider')}></div>
            </div>
            <span className={b('author')}>{filler.author}</span>
            <p className={b('text')}>{filler.text}</p>
          </div>
      )
    } 
}