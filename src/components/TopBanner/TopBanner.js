import React, { Component, PropTypes as Type } from 'react'
import block from 'bem-cn';
import './TopBanner.styl';
import src from 'assets/banners/top_baner_bg.jpg';
let b = block('top-banner');

export default class TopBanner extends Component {

	constructor(props) {
		super(props);

	}
    render() {
    	return (
      		<div className={b} style={ { backgroundImage: `url(${src})`} }>
            <div className={b('mask')}></div>
            <div className={b('flex-block')}>
              <div className={b('item')}>
                <span>Культурный минимум</span>
                <div className={b('icon')}></div>
                <span>Лекторий</span>
              </div>
              <div className={b('item')}>
                <p>Серия лекций с авторитетными спикерами, настоящими профессионалами в своих областях</p>
              </div>
            </div>
      		</div>
    	)
  	}	
}