import './InitialContainer.styl';

import React, {
  Component,
  PropTypes as Type,
} from 'react';

import { bindActionCreators }       from 'redux';
import { connect }                  from 'react-redux';

import {
  TestActions,
} from 'actions';

import { TopBanner, Head, InterviewBlock, Blog, Analytics, Comments, Button, Social, Foot, Important} from 'components';


@connect(state => ({
  testData: state.testReducer
}), dispatch => ({
  actions: bindActionCreators(TestActions, dispatch)
}))

export default class InitialContainer extends Component {
  /**
   * Validates passed properties
   */
  static propTypes = {
    actions: Type.object,
    testData: Type.object
  };

  /**
   * Invokes after the initial rendering of component
   */
  componentDidMount() {
    this.props.actions.testAction();
  }

  /**
   * Renders 'InitialContainer' component
   */
  render() {
    console.log('this.props.testData.message ==>', this.props.testData.message);
    const blog = [
      {
        img: '', 
        title: 'РФ обещает сохранить объем поддержки для АПК, аграрии просят дать им неиспользуемые земли',
        text: 'В прошлом году на финансирование аграрной отрасли было направлено 237 млрд рублей. В 2016 году объем финансовой поддержки был снижен на 10% в рамках оптимизации бюджета',
        authorImg: '',
        author: 'Константин Лебедев',
        author_position: 'финансовый эксперт'
      },
      {
        img: '', 
        title: 'Противоядие для западных рубежей: чем РФ ответит на усиление баз НАТО в Восточной Европе',
        text: 'Речь идёт о заявлении министра обороны России Сергея Шойгу, сделанном на первом в этом году селекторном совещании военного ведомства, о важнейшей задаче нынешнего года – формировании трех дивизий на западном направлении.',
        authorImg: '',
        author: 'Николай Кривоконь',
        author_position: 'военный эксперт'
      },
      {
        img: '', 
        title: 'ЕГЭ по математике базового уровня сдали 80% выпускников',
        text: 'Речь идёт о заявлении министра обороны России Сергея Шойгу, сделанном на первом в этом году селекторном совещании военного ведомства, о важнейшей задаче нынешнего года – формировании трех дивизий на западном направлении.',
        authorImg: '',
        author: 'Александр Ходасевич',
        author_position: 'военный эксперт'
      }
    ]

    return (
      <div className="root">
        <TopBanner />
        <Head />
        <InterviewBlock />
        <Blog item={blog[0]}/>
        <Analytics />
        <Important />
        <Blog item={blog[1]}/>
        <Comments />
        <Blog item={blog[2]}/>
        <Button />
        <Social />
        <Foot />
      </div>
    );
  }
}
