// Webpack config for development
var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname, '..'),
  devtool: 'cheap-eval-source-map',
  entry: {
    'main': [
      'webpack-hot-middleware/client',
      './src/app.js'
    ]
  },
  output: {
    path: path.resolve(__dirname),
    filename: 'main.js'
  },
  module: {
    loaders: [
      { test: /\.svg$/,
        loader: 'babel!svg-react'
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
        test: /\.styl|\.css$/,
        loader: 'style'
      },
      {
        test: /\.styl|\.css$/,
        loader: 'css',
        query: {
          importLoaders: 1,
          localIdentName: '[local]',
          modules: true,
          sourceMap: true,
        }
      },
      {
        test: /\.styl|\.css$/,
        exclude: /node_modules/,
        loader: 'stylus',
        query: {
          sourceMap: true
        }
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$/i,
        loader: 'file?name=[path][name].[ext]'
      }
    ]
  },
  resolve: {
    modulesDirectories: [
      'src',
      'node_modules'
    ],
    extensions: ['', '.json', '.js', '.jsx']
  },
  plugins: [
    new webpack.DefinePlugin({
      'STAGING': process.env.NODE_ENV === 'staging',
      'PRODUCTION': process.env.NODE_ENV === 'production',
      '__DEVELOPMENT__': process.env.NODE_ENV === 'development',
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en-gb/),
    new webpack.HotModuleReplacementPlugin()
  ],
  target: 'web', // Make web variables accessible to webpack, e.g. window
  stats: true, // Don't show stats in the console
  progress: true
};
